{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  };
  outputs = inputs@{ nixpkgs, ... }:
    let
      eachSystem = systems: f:
        let
          op = attrs: system:
            let
              ret = f system;
              op = attrs: key:
                let
                  appendSystem = key: system: ret: { ${system} = ret.${key}; };
                in attrs // {
                  ${key} = (attrs.${key} or { })
                    // (appendSystem key system ret);
                };
            in builtins.foldl' op attrs (builtins.attrNames ret);
        in builtins.foldl' op { } systems;
      defaultSystems = [ "aarch64-linux" ];
    in eachSystem defaultSystems (system:
      let
        pkgs = import nixpkgs { inherit system; };
      in rec {
        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [
            cmake
            gcc
            python3
            alsa-lib
            libGL
            libGLU
            libglvnd
            libglibutil
            xorg.libX11
            xorg.xrandr
            xorg.libXrandr
            xorg.libXinerama
            xorg.libXcursor
            libjack2
            zlib
            sqlite
            libpkgconf
            pkg-config
            rtmidi
            glfw
          ];
        };
      });
}
